#!/bin/bash

BULLET_NATIVE_PATH='C:\dev\projects\vs10\android\bullet\bullet-jni\BulletLib'

bulletNativePath=''
if [ -z $1 ]; then
	bulletNativePath=$BULLET_NATIVE_PATH
else
	bulletNativePath=$1
fi

echo "Running javah and copying header to: " $bulletNativePath

cd bin
# javah -jni bullet.BulletLib
javah -classpath '.;../../../java-android/def-shared/bin' -jni bullet.BulletLib
rm $BULLET_NATIVE_PATH\\bullet_BulletLib.h
cp -f bullet_BulletLib.h $BULLET_NATIVE_PATH
cd ..
