package bullet;

import def.shared.interfaces.ICollisonReporter;
import def.shared.interfaces.IRayHitReporter;
import bullet.adapter.CollisionShapeConstuctionInfo;

public class BulletLib {
	public static native void initSimulationWithoutGround();
	
	public static native void initSimulation(final long groundYPos); 
	
	public static native void simulate(final long timeStep);
	
	public static native float[] getGravity();
	
	public static native void setGravity(float[] gravity);
	
	public static native void shootRay(float[] from, float[] to, IRayHitReporter callback);
	
	public static native long addRigidBody(float[] pos, CollisionShapeConstuctionInfo colShape, float mass);
	
	public static native boolean removeRigidBody(long id);
	
	// Set transforms for all graphical representations and update collsions
	public static native void syncVisuals(ICollisonReporter reporter);
	
	static
	{
	    final String[] libs = {
	    	"BulletLib"
	    };

	    String searchpath = System.getProperty("java.library.path");
	    System.out.println("Searching for libs using: " + searchpath);
	    for (int i = 0;  i < libs.length; ++i)
	    {
	    	try{
	    		System.out.println("Loading " + libs[i] + "...");
	    		System.loadLibrary(libs[i]);
	    	}catch(UnsatisfiedLinkError e){
	    		System.err.println("Failed loading lib " + libs[i] + ". Msg: " + e.getMessage());
	    	}
	    }
	}

}
