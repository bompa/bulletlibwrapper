package bullet.adapter;

import def.shared.interfaces.IPhysicsObjectData;

public class CollisionShapeConstuctionInfo{
	public static CollisionShapeConstuctionInfo createCubeUnitSize()
	{
		return createCube(new float[]{1.0f, 1.0f, 1.0f});
	}
	
	public static CollisionShapeConstuctionInfo createCube(float[] dimensions) {
		if(dimensions == null)
			throw new IllegalArgumentException("Failed creating CollisionShapeConstuctionInfo, dimensions=null");
		
		CollisionShapeConstuctionInfo info = new CollisionShapeConstuctionInfo();
		info.type = Type.CUBE;
		info.dimensions = dimensions;
		return info;
	}
	
	public static CollisionShapeConstuctionInfo createSphere(float radius) {
		if(radius <= 0)
			throw new IllegalArgumentException("Failed creating CollisionShapeConstuctionInfo, radius<=0");
		
		CollisionShapeConstuctionInfo info = new CollisionShapeConstuctionInfo();
		info.type = Type.SPHERE;
		info.dimensions = new float[]{radius, radius, radius};
		return info;
	}
	
	private CollisionShapeConstuctionInfo(){}
	
	public enum Type
	{	
		CUBE("CUBE"), 
		SPHERE("SPHERE");
		
		public String getName(){
			return n;
		}
		
		private Type(String n){
			this.n = n;
		}
		
		private final String n;		
	}
	
	public float dimensions[];
	public Type type;
	public IPhysicsObjectData physicsObjData = null;
}

