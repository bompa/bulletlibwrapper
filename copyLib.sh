#!/bin/bash

bulletNativePath=''
if [ -z $1 ]; then
	echo 'Argument target missing. Can be Release/Debug.'
	exit
fi

cp -v -f /c/dev/projects/vs10/android/bullet/build/vs2010/$1/BulletLib.dll /c/dev/projects/java/bulletlibtestloader/lib
cp -v -f /c/dev/projects/vs10/android/bullet/build/vs2010/$1/libBulletLib.so /c/dev/projects/java-android/def20/libs/armeabi
