#!/bin/bash

package=$1

if [ -z $package ]; then
	echo 'Missing package parameter'
	exit
fi


cd bin/$package
for f in *.class
do
	echo  "*Signatures for $f*"
	echo ""
	javap -s $f
done